#!/bin/bash

# Exit script on any error
set -e 

#=====================================
# Color Settings:
#=====================================
NC='\033[0m'
OUTPUT='\033[0;32m'
WARNING='\033[0;93m'

echo -e "${OUTPUT}"
echo "=============================================================================="
echo "Running cppcheck"
echo "=============================================================================="
echo -e "${NC}"
echo "Started: $(date)"
echo "Please Wait ..."

# Run cppcheck and output into file
cppcheck  --enable=warning,style,performance,information \
          -j4 \
          --quiet \
          --force \
          -Umin -Umax -UBMPOSTFIX \
          src libs/glow/src/ libs/aion/src libs/glow-extras &> cppcheck.log

echo "Finished: $(date)"

echo -e "${OUTPUT}"
echo "=============================================================================="
echo "CPPCHECK Messages"
echo "=============================================================================="
echo -e "${NC}"


# Echo output to command line for simple analysis via gitlab
cat cppcheck.log

#full line count
COUNT=$(wc -l < cppcheck.log )
ERRCOUNT=$(echo `grep -c "(error)" cppcheck.log`)
WARCOUNT=$(echo `grep -c "(warning)" cppcheck.log`)
INFCOUNT=$(echo `grep -c "(information)" cppcheck.log`)
STYCOUNT=$(echo `grep -c "(style)" cppcheck.log`)
PERCOUNT=$(echo `grep -c "(performance)" cppcheck.log`)


echo -e "${OUTPUT}"
echo "=============================================================================="
echo "CPPCHECK Summary"
echo "=============================================================================="
echo -e "${NC}"

echo "Total CPPCHECK Warning Count is $WARCOUNT"
echo "Total CPPCHECK Information Count is $INFCOUNT"
echo "Total CPPCHECK Style Count is $STYCOUNT"
echo "Total CPPCHECK Performance Count is $PERCOUNT"

MAX_ERROR=100
if [ $ERRCOUNT -gt $MAX_ERROR ]; then
  echo -e ${WARNING}
  echo "Total CPPCHECK error Count is $ERRCOUNT, which is too High (Max is $MAX_ERROR)! CPPCHECK Run failed";
  echo -e "${NC}"
  exit 1;
else
  echo "Total CPPCHECK error Count is $ERRCOUNT ... OK"
fi

 
